package com.les_danseurs_lyonnais.projectldl.test_entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import com.les_danseurs_lyonnais.projectldl.dao.UserRepo;
import com.les_danseurs_lyonnais.projectldl.model.User;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

@SpringBootTest
@Profile("test")
@Sql(scripts = {"/schema.sql","/data.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
public class UserRepoTest {
    
    @Autowired
    private UserRepo repo;


    @Test
    public void testFindAll() {
        List<User> result = repo.findAll();
        assertEquals(1, result.size());
    }

    @Test
    public void testFindById() {
        User toFind = repo.findById(1);
        assertTrue(toFind.getId()==1);
    }

    @Test
    public void testSave() {
        User toAdd = new User();
        repo.save(toAdd);
        assertNotNull(toAdd.getId()==2);
    }

}
