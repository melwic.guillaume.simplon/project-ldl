package com.les_danseurs_lyonnais.projectldl.controller;

import com.les_danseurs_lyonnais.projectldl.dao.UserRepo;
import com.les_danseurs_lyonnais.projectldl.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
    @Autowired
    private UserRepo uRepo;

    @GetMapping("/users")
    public String showUsers(Model model) {
        model.addAttribute("user", uRepo.findAll());
        return "/users";
    }

    @GetMapping("/new_user")
    public String showAddForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "add_user";
    }

    @PostMapping("/add_user")
    public String addSubscriber(@ModelAttribute("subscriber") User user, Model model) {
       uRepo.save(user);
       return "redirect:/users";
    }
}
