package com.les_danseurs_lyonnais.projectldl.controller;

import com.les_danseurs_lyonnais.projectldl.dao.StyleRepo;
import com.les_danseurs_lyonnais.projectldl.model.Style;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller

public class StyleController {
    @Autowired
    private StyleRepo sRepo;

    @GetMapping("/styles")
    public String showStyles(Model model) {
        model.addAttribute("style", sRepo.findAll());
        return "/styles";
    }

    @GetMapping("/new_style")
    public String showAddForm(Model model) {
        Style style = new Style();
        model.addAttribute("style", style);
        return "add_style";
    }

    @PostMapping("/add_style")
    public String addSubscriber(@ModelAttribute("subscriber") Style style, Model model) {
       sRepo.save(style);
       return "redirect:/styles";
    }
}
