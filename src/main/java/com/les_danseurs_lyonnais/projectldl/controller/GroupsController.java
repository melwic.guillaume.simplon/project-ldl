package com.les_danseurs_lyonnais.projectldl.controller;

import com.les_danseurs_lyonnais.projectldl.dao.GroupsRepo;
import com.les_danseurs_lyonnais.projectldl.model.Groups;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GroupsController {
    @Autowired
    private GroupsRepo gRepo;

    @GetMapping("/groups")
    public String showGroups(Model model) {
        model.addAttribute("groups", gRepo.findAll());
        return "/groups";
    }

   
    @GetMapping("/edit/{id}")
    public ModelAndView showEditForm(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_groups");
        Groups groups = gRepo.findById(id);
        mav.addObject("groups", groups);
        return mav;
    }

   
    @PostMapping("/update/{id}")
    public String updateGroupsById(@ModelAttribute("groups") Groups groups, @PathVariable(name = "id") int id) {
        groups.setId(id);
        gRepo.update(groups);
        return "redirect:/groups";
    }
}
