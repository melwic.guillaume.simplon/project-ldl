package com.les_danseurs_lyonnais.projectldl.controller;

import com.les_danseurs_lyonnais.projectldl.dao.PostRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller

public class PostController {
    @Autowired
    private PostRepo pRepo;

    @GetMapping("/posts")
    public String showPosts(Model model) {
        model.addAttribute("post", pRepo.findAll());
        return "/posts";
    }

    @GetMapping(value = "/delete/{id}", produces = "application/json")
    public String deleteSubscriberById(@PathVariable(name = "id") int id) {
	    pRepo.deleteById(id);
	    return "redirect:/posts";       
	}
}
