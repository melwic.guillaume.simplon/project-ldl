package com.les_danseurs_lyonnais.projectldl.dao;

import java.util.List;

import com.les_danseurs_lyonnais.projectldl.model.Style;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StyleRepo extends JpaRepository<Style, Integer> {

    List<Style>findByUsers_Id(Integer userId);
}
