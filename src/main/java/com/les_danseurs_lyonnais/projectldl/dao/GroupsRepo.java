package com.les_danseurs_lyonnais.projectldl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.les_danseurs_lyonnais.projectldl.model.Groups;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

@Repository
public class GroupsRepo implements IRepository<Groups> {

    @Autowired
    private DataSource dataSource;
    @Autowired
    @Lazy
    private UserRepo users;
    private Connection cnx;

    private static final String GET_GROUPS = "SELECT * FROM groups";
    private static final String GET_GROUP_BY_ID = "SELECT * FROM groups WHERE id=?";
    private static final String ADD_GROUP = "INSERT INTO groups (group_photo, group_name, group_type, group_description) VALUES (?,?,?,?)";
    private static final String UPDATE_GROUP_BY_ID = "UPDATE groups SET group_photo = ?, group_name = ?, group_type = ?, group_description = ? WHERE id = ?";
    private static final String DELETE_GROUP_BY_ID = "DELETE FROM groups WHERE id = ?";
    private static final String GET_USER_GROUPS ="SELECT * FROM groups LEFT JOIN groups_users ON groups.id=groups_users.groups_id WHERE groups_users.users_id=?";


    @Override
    public List<Groups> findAll() {
        List<Groups> listGroup = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_GROUPS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Groups groups = new Groups(
                    rs.getInt("id"),
                    rs.getString("group_photo"),
                    rs.getString("group_name"),
                    rs.getString("group_type"),
                    rs.getString("group_description")
                    );
                    listGroup.add(groups);
                    groups.setUsers(users.findAllGroupUsers(rs.getInt("id")));
            }
            cnx.close();
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        
        }
        return listGroup;       
    }

    @Override
    public Groups findById(int id) {
        Groups group = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_GROUP_BY_ID);			
            ps.setInt(1, id);			
            ResultSet rs = ps.executeQuery();			
            if (rs.next()) {				
                group = new Groups();
                group.setId(rs.getInt("id"));;
                group.setGroup_photo(rs.getString("group_photo"));;
                group.setGroup_name(rs.getString("group_name"));
                group.setGroup_type(rs.getString("group_type"));;
                group.setGroup_description(rs.getString("group_description"));;
            }
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return group;
    }

    @Override
    public boolean save(Groups model) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(ADD_GROUP);
            ps.setString(1, model.getGroup_photo());
            ps.setString(2, model.getGroup_name());
            ps.setString(3, model.getGroup_type());
            ps.setString(4, model.getGroup_description());
            ps.executeUpdate();
            cnx.close();         
        } catch (Exception e) {          
            e.getMessage();
        } finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
    }

    @Override
    public boolean update(Groups model) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(UPDATE_GROUP_BY_ID);
            ps.setString(1, model.getGroup_photo());
            ps.setString(2, model.getGroup_name());
            ps.setString(3, model.getGroup_type());
            ps.setString(4, model.getGroup_description());
            ps.setInt(5, model.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(DELETE_GROUP_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
    }
    
    
    public List<Groups> findAllUserGroups(Integer userId) {
        List<Groups> groupsList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_USER_GROUPS);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
               groupsList.add(
                   new Groups(
                       rs.getInt("id"),
                       rs.getString("group_name")
                       )
                    );
            }
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return groupsList;
    }

}
