package com.les_danseurs_lyonnais.projectldl.dao;

import java.util.List;

public interface IRepository<T> {
    List <T> findAll();
    T findById(int id);
    boolean save(T model);
    boolean update(T model);
    boolean deleteById(Integer id);
}
