package com.les_danseurs_lyonnais.projectldl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.les_danseurs_lyonnais.projectldl.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepo implements IRepository<User> {

    @Autowired
    private DataSource dataSource;
    @Autowired
    @Lazy
    private GroupsRepo groups;
    @Autowired
    @Lazy
    private StyleRepo styles;
    private Connection cnx;

    private static final String GET_USERS = "SELECT * FROM user";
    private static final String GET_USER_BY_ID = "SELECT * FROM user WHERE id=?";
    private static final String ADD_USER = "INSERT INTO user (photo, name, nick_name, email, password, description) VALUES (?,?,?,?,?,?)";
    private static final String UPDATE_USER_BY_ID = "UPDATE user SET photo = ?, name = ?, nick_name = ?, email = ?, password = ?, description = ? WHERE id = ?";
    private static final String DELETE_USER_BY_ID = "DELETE FROM user WHERE id = ?";
    private static final String GET_GROUP_USERS ="SELECT * FROM user LEFT JOIN groups_users ON user.id=groups_users.users_id WHERE groups_users.groups_id=?";


    @Override
    public List<User> findAll() {
        List<User> listUser = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_USERS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user=new User(
                    rs.getString("photo"),
                    rs.getString("name"),
                    rs.getString("nick_name"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("role"),
                    rs.getString("description"),
                    rs.getString("group_role")
                );
                listUser.add(user);
                user.setGroups(groups.findAllUserGroups(rs.getInt("id")));
                user.setStyles(styles.findByUsers_Id(rs.getInt("id")));
            }
            cnx.close();
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        
        }
        return listUser;
    }

    @Override
    public User findById(int id) {
        User user = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_USER_BY_ID);			
            ps.setInt(1, id);			
            ResultSet rs = ps.executeQuery();			
            if (rs.next()) {				
                user = new User();
                user.setId(rs.getInt("id"));;
                user.setPhoto(rs.getString("photo"));
                user.setName(rs.getString("name"));
                user.setNick_name(rs.getString("nick_name"));;
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
            }
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return user;
    }

    @Override
    public boolean save(User model) {     
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(ADD_USER);
            ps.setString(1, model.getPhoto());
            ps.setString(2, model.getName());
            ps.setString(3, model.getNick_name());
            ps.setString(4, model.getEmail());
            ps.setString(5, model.getPassword());
            ps.setString(6, model.getDescription());
            ps.executeUpdate();
            cnx.close();         
        } catch (Exception e) {          
            e.getMessage();
        } finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
}

    @Override
    public boolean update(User model) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(UPDATE_USER_BY_ID);
            ps.setString(1, model.getPhoto());
            ps.setString(2, model.getName());
            ps.setString(3, model.getNick_name());
            ps.setString(4, model.getEmail());
            ps.setString(5, model.getPassword());
            ps.setString(6, model.getDescription());
            ps.executeUpdate();
        } catch (Exception e) {
            e.getMessage();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(DELETE_USER_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally { 
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return false;
    }

    public List<User> findAllGroupUsers(Integer userId) {
        List<User> userList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_GROUP_USERS);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                userList.add(
                   new User(
                       rs.getInt("id"),
                       rs.getString("name"),
                       rs.getString("group_role")
                       )
                    );
            }
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return userList;
    }
}
