package com.les_danseurs_lyonnais.projectldl.dao;


import com.les_danseurs_lyonnais.projectldl.model.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepo extends JpaRepository<Post, Integer> {
}
