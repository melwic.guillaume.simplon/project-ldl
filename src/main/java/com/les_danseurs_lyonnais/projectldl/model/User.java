package com.les_danseurs_lyonnais.projectldl.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String photo;
    private String name;
    private String nick_name;
    private String email;
    private String password;
    private String role;
    private String description;
    private String group_role;
    @OneToMany(mappedBy = "user")
    private List<Post> posts = new ArrayList<>();
    @ManyToMany(mappedBy = "users")
    private List<Style> styles = new ArrayList<>();
    @ManyToMany(mappedBy = "users")
    private List<Groups> groups = new ArrayList<>();


    
    public User(int id, String name, String group_role) {
        this.id = id;
        this.name = name;
        this.group_role = group_role;
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(int id, String photo, String name, String nick_name, String email, String password, String role,
            String description, String group_role, List<Style> styles, List<Groups> groups) {
        this.id = id;
        this.photo = photo;
        this.name = name;
        this.nick_name = nick_name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.description = description;
        this.group_role = group_role;
        this.styles = styles;
        this.groups = groups;
    }

    public User(String photo, String name, String nick_name, String email, String password, String role,
            String description, String group_role, List<Style> styles, List<Groups> groups) {
        this.photo = photo;
        this.name = name;
        this.nick_name = nick_name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.description = description;
        this.group_role = group_role;
        this.styles = styles;
        this.groups = groups;
    }

    public User(int id, String photo, String name, String nick_name, String email, String password, String role,
            String description, String group_role) {
        this.id = id;
        this.photo = photo;
        this.name = name;
        this.nick_name = nick_name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.description = description;
        this.group_role = group_role;
    }

    public User(String photo, String name, String nick_name, String email, String password, String role,
            String description, String group_role) {
        this.photo = photo;
        this.name = name;
        this.nick_name = nick_name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.description = description;
        this.group_role = group_role;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroup_role() {
        return group_role;
    }

    public void setGroup_role(String group_role) {
        this.group_role = group_role;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Style> getStyles() {
        return styles;
    }

    public void setStyles(List<Style> styles) {
        this.styles = styles;
    }

    public List<Groups> getGroups() {
        return groups;
    }

    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

}
