package com.les_danseurs_lyonnais.projectldl.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Style {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String style_name;
    @ManyToMany
    private List<User> users = new ArrayList<>();
    
    public Style(String style_name) {
        this.style_name = style_name;
    }
    public Style(int id, String style_name) {
        this.id = id;
        this.style_name = style_name;
    }
    public Style() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getStyle_name() {
        return style_name;
    }
    public void setStyle_name(String style_name) {
        this.style_name = style_name;
    }

}
