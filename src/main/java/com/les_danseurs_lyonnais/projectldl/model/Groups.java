package com.les_danseurs_lyonnais.projectldl.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Groups {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String group_photo;
    private String group_name;
    private String group_type;
    private String group_description;
    @ManyToMany
    private List<User> users = new ArrayList<>();

    public Groups(int id, String group_name) {
        this.id = id;
        this.group_name = group_name;
    }


    public Groups(String group_photo, String group_name, String group_type, String group_description) {
        this.group_photo = group_photo;
        this.group_name = group_name;
        this.group_type = group_type;
        this.group_description = group_description;
    }


    public Groups(int id, String group_photo, String group_name, String group_type, String group_description) {
        this.id = id;
        this.group_photo = group_photo;
        this.group_name = group_name;
        this.group_type = group_type;
        this.group_description = group_description;
    }


    public Groups() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroup_photo() {
        return group_photo;
    }

    public void setGroup_photo(String group_photo) {
        this.group_photo = group_photo;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }


    public void setGroups(List<User> findAllGroupUsers) {
    }


    public List<User> getUsers() {
        return users;
    }


    public void setUsers(List<User> users) {
        this.users = users;
    }

}
