package com.les_danseurs_lyonnais.projectldl.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String post;
    private String article_post;
    @ManyToOne
    private User user;
    
    public Post(String post, String article_post, User user) {
        this.post = post;
        this.article_post = article_post;
        this.user = user;
    }
    public Post(int id, String post, String article_post, User user) {
        this.id = id;
        this.post = post;
        this.article_post = article_post;
        this.user = user;
    }
    public Post(String post, String article_post) {
        this.post = post;
        this.article_post = article_post;
    }
    public Post(int id, String post, String article_post) {
        this.id = id;
        this.post = post;
        this.article_post = article_post;
    }
    public Post() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getPost() {
        return post;
    }
    public void setPost(String post) {
        this.post = post;
    }
    public String getArticle_post() {
        return article_post;
    }
    public void setArticle_post(String article_post) {
        this.article_post = article_post;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    
}