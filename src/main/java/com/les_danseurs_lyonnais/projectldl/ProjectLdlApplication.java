package com.les_danseurs_lyonnais.projectldl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectLdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectLdlApplication.class, args);
	}

}
