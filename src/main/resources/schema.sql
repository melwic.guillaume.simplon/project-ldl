DROP TABLE IF EXISTS user_groups;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS user;
CREATE TABLE user(
  id_user INTEGER PRIMARY KEY AUTO_INCREMENT,
  photo VARCHAR(255),
  name VARCHAR(255) NOT NULL,
  nick_name VARCHAR(255),
  email VARCHAR(255) UNIQUE NOT NULL,
  password VARCHAR(255),
  role VARCHAR(255) DEFAULT 'ROLE_USER',
  description TEXT,
  group_role VARCHAR(255) DEFAULT 'ROLE_MEMBER'
);
CREATE TABLE groups(
  id_group INTEGER PRIMARY KEY AUTO_INCREMENT,
  group_photo VARCHAR(255) NULL,
  group_name VARCHAR(255) NOT NULL,
  group_type VARCHAR(255) NULL,
  group_role VARCHAR(255) DEFAULT 'ROLE_MEMBER',
  group_description TEXT
);
CREATE TABLE user_groups(
    id_user INTEGER,
    id_group INTEGER,
    PRIMARY KEY(id_user, id_group),
    FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
    FOREIGN KEY (id_group) REFERENCES groups(id_group) ON DELETE CASCADE
  );