DROP DATABASE IF EXISTS les_danseurs_lyonnais;
CREATE DATABASE les_danseurs_lyonnais;
USE les_danseurs_lyonnais;
DROP TABLE IF EXISTS user_groups;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS user;
CREATE TABLE user(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  photo VARCHAR(255),
  name VARCHAR(255) NOT NULL,
  nick_name VARCHAR(255),
  email VARCHAR(255) UNIQUE NOT NULL,
  password VARCHAR(255),
  role VARCHAR(255) DEFAULT 'ROLE_USER',
  description TEXT,
  group_role VARCHAR(255) DEFAULT 'ROLE_MEMBER'
);
CREATE TABLE groups(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  group_photo VARCHAR(255) NULL,
  group_name VARCHAR(255) NOT NULL,
  group_type VARCHAR(255) NULL,
  group_description TEXT
);
CREATE TABLE groups_users(
    users_id INTEGER,
    groups_id INTEGER,
    PRIMARY KEY(users_id, groups_id),
    FOREIGN KEY (users_id) REFERENCES user(id) ON DELETE CASCADE,
    FOREIGN KEY (groups_id) REFERENCES groups(id) ON DELETE CASCADE
  );
INSERT INTO
  user (name, nick_name, email, role, group_role)
VALUES(
    "Melwic",
    "Welmic",
    "melwic.g@gmail.com",
    "ROLE_ADMIN",
    NULL
  );
    INSERT INTO
  user (name, nick_name, email)
VALUES(
    "Brian",
    "Boycott",
    "boycott@gmail.com"
  );